#!/usr/bin/env bash
# Why use a bash script for upstart you may ask, this is the reason:
# 1. It is usable across all windowmanagers
# 2. It lets you run different programs on different conditions,  for an example at waybar and gammastep only running when wayland
# 3. you can also set environment variables


# for emacs use plists which is according to Emacs Wiki gives better perfomance for emacs lsp client
export LSP_USE_PLISTS=true

# only start process if it's not already running
# this is a bashism, so it won't work on dash/sh,
# "pgrep" gives the PID of a  process, -f means list name instead of PID,
# "$1" is 2.st argument as the counting starts from 0
 function run {
    if ! pgrep -x "$1" >/dev/null
    then
        "$@" &
    fi
}

# display server agnostic key rebinder and command binder.
# does not start if I use autorun.sh () ?
kmonad ~/.config/kmonad/config.kbd & disown
# run an instance for razer keyboard, Im not goin to use an if statement right now
kmonad ~/.config/kmonad/config-razer-blackwidow_x_chroma.kbd & disown
echo "if two failure then razer keyboard was not used"

# run() in posix shell, it is not really dependable sadly.
# run() {
#   if  [  "" = "$(pgrep -f $1))" ]
#   then $@&
#   fi
# }

######################################
# services and daemons #
######################################


	# needs to be done for switch keyboard scrit to work
	# run redshift -m drm -PO 3000
	# run gammastep
	# daemons letting program run in background
	run thunar --daemon || run nautilus

	# syncronises files between pc's decentralised, privately and securely this is better (in my opinion) than google drive and nextcloud as it's very simple to set up.but it kills battery.... normally i have [6-8]*45min battery, but with this  it barely had 4*45 min.... it takes a 1/3 battery... pathetic
	# run syncthing -allow-newer-config

	run ibus-daemon -d

#####################
# statusbar applets #
#####################

	# shows network info in statusbar, works in wayland and on
	run nm-applet # wifi info

	# show frequency of cpu and cpu governor(pretty useless for most people)
	# run indicator-cpufreq

####################
# startup programs #
####################

	#mail
	#run thunderbird


	#emac is an exelentlent environment, emacs started, this speeds up emacs startup by alot much(emacs is thee editor)
	# run emacs --bg-daemon &&  emacsclient --create-frame &


	# fcitx5 for input methods, like japanese, or instead use emacs or ibus on gnome, where it is intergrated nicely
	# run fcitx5
	run firefox
	#  on arch or linux mint, anki needs this to show decks at the startscreen,
	#  this does not declare a global variable, it sets a local variable for the created anki process
        env QTWEBENGINE_CHROMIUM_FLAGS="--no-sandbox" anki & disown
	# idk why is has to be graphical to start singal, does not make any difference as signal is always slow to startup
	run signal-desktop || sleep 1 && flatpak run org.signal.Signal & disown
	# run krita
	# private end to end encrypted messenger, which can also use sms on phones.
	# run signal-desktop

	# anki is spaced repetition program, it is really good for memorizing non-logical info or info which is unrelateable.
	# an example is chinese characters or subject matters like ribosome or which year the usa was founded.
	# anki --no-sandbox &
	# run anki --no-sandbox
	# I use anki-editor in emacs for visual card creation as that allows fast
	# creation and editing via vim keys,  vim plugins do not allow preview
	# of images which I use for context.

##########################################
# wayland on wl-roots based compositors	 #
##########################################


# programs that only need to be run on wayland
if [ $XDG_SESSION_TYPE == wayland ]; then
# wayland statusbar
	# gammatep only works on wlroots based compositors only, am using Gnome
	# as of november 2023 as it supports japanese input method.
	# run gammastep -m wayland

	# this is not needed when running sway, it is better to let sway create the process
	# run waybar

	# QT_IM_MODULE=fcitx # For those who bundle qt im module, e.g. WPS, Anki, you should find a .so file with fcitx in the file name
	# QT_IM_MODULE=ibus # For those who bundle ibus im module shipped with Qt, you should find libibusplatforminputcontextplugin.so in the package.
	# QT_QPA_PLATFORM=xcb QT_IM_MODULE=ibus # Enforce it to run on X11/XWayland and use ibus im module
	echo "nothing wayland right now"


else
# This is programs that only run on XORG/X11

	# step screen color according to sun like F.lux or "night light" and redshift
	# gammastep has wayland support from source, that is why
	run gammastep -m randr

	# needed for my keyswitcher kmonad script to work, idk why
	run setxkbmap us & disown
	#get id  number for touchpad for setting settings
	ID=$(xinput list | grep Touch | fold -w 8 | grep id | sed "s/id=//")
	# tapping
	xinput --set-prop $ID "libinput Tapping Enabled" 1
	#also set touchpad sensetivity.
	# xinput --set-prop ${ID} 'libinput Accel Speed' 0.25
	xinput --set-prop $ID "libinput Accel Speed" 0.25
	#disable mouse accelaration
	xinput --set-prop $ID  "Coordinate Transformation Matrix" 2.4 0 0 0 2.4 0 0 0 1

	#java does not work in dwm because it is non-parenting (apparently), this does something that  this fixes it(found on arch wiki)
	#export AWT_TOOLKIT=MToolkit
	# but sometimes the fix does still not resolve the issue

	#DPMS, screen blankout time, to 60 seconds or 1 minute
	# WORKS ONLY FOR X11, YOU HAVE TO USE A DIFFERENT UTILITY/PROGRAM ON WAYLAND
	xset s 120
#set option of swapping escape and capslog, usefull when using vim, and can be disabled by passing having only "setxkbmap -option" to a terminal
# exec setxkbmap -option caps:swapescape
# executing them stops the script

# remove any setxkmap key replacements, the command is postponed for 10 seconds, as xorg has to start for the command to have an effect.
#	sleep 10 && setxkbmap -option & setxkbmap dk &
#        sleep 10 setxkbmap -layout "dk,us" -option "grp:alt_shift_toggle" -option caps:swapescape &

	# run A X11 compitor so no screentearing occours, Enables Vsync so not ideal for competitive gaming
	run picom
 fi

