;;;t$DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; org-clock display is very nice

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name ""
      user-mail-address "")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
;;(setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'semi-light)
;;      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 13))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-gruvbox)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'relative)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
;; make pirectory, t means: if the parent directory does not exits also make it.
;; (make-directory "~/Documents/org/roam" t)
;; (setq org-directory  "~/Documents/org" )
;; (setq org-roam-directory "~/Documents/org/roam")

;; which files the org-agenda view shall check for  todo and calendar
;; I do not know why only
;; (setq org-agenda-files "~/documents/org") ; does not apply for every file in the directory, but this works.
;; (after! org (setq org-agenda-files (apply 'append
;; 			      (mapcar
;; 			       (lambda (directory)
;; 				 (directory-files-recursively
;; 				  directory org-agenda-file-regexp))
;; 			       '("~/Documents/org" )))))
(custom-set-variables
 '(org-directory "~/Documents/org")
 '(org-agenda-files (list org-directory))
 ;; '(org-download-screenshot-method "grim -g \"$(slurp)\" %s")
 )
;; (setq org-agenda-files "~/Documents/org/todo.org") ;; just point at the todo file
;; , and the Org-capture fuction automatically points
;; for todo headers

;; non doom standard todo instead of *[ ], as todo can be recognized by Org-agenda
	  ;; (after! org (setq org-capture-templates
;; '(("t" "Personal todo" entry
;;   (file+headline +org-capture-todo-file "Inbox")
;;   ;; "* TODO %?\n%i\n%a" :prepend t)
;;   "* TODO %?\n %i\n %a")
;;  ("n" "Personal notes" entry
;;   (file+headline +org-capture-notes-file "Inbox")
;;   "* %u %?\n%i\n%a" :prepend t)
;;  ("j" "Journal" entry
;;   (file+olp+datetree +org-capture-journal-file)
;;   "* %U %?\n%i\n%a" :prepend t)
;;  ("p" "Templates for projects")
;;  ("pt" "Project-local todo" entry
;;   (file+headline +org-capture-project-todo-file "Inbox")
;;   "* TODO %?\n%i\n%a" :prepend t)
;;  ("pn" "Project-local notes" entry
;;   (file+headline +org-capture-project-notes-file "Inbox")
;;   "* %U %?\n%i\n%a" :prepend t)
;;  ("pc" "Project-local changelog" entry
;;   (file+headline +org-capture-project-changelog-file "Unreleased")
;;   "* %U %?\n%i\n%a" :prepend t)
;;  ("o" "Centralized templates for projects")
;;  ("ot" "Project todo" entry #'+org-capture-central-project-todo-file "* TODO %?\n %i\n %a" :heading "Tasks" :prepend nil)
;;  ("on" "Project notes" entry #'+org-capture-central-project-notes-file "* %U %?\n %i\n %a" :heading "Notes" :prepend t)
;;  ("oc" "Project changelog" entry #'+org-capture-central-project-changelog-file "* %U %?\n %i\n %a" :heading "Changelog" :prepend t)))
;; )


(setq org-todo-keywords
           '((sequence "TODO(t)" "|" "DONE(d)" "CANCELED(c)")
             (sequence "REPORT(r)" "BUG(b)" "KNOWNCAUSE(k)" "|" "FIXED(f)")
             (sequence "PREPARE(p)" "|" "ACTIVE(a)" "|" "DONE(d)")
             ))

;; color and font idk. I need to add some entries for coloring above
(setq hl-todo-keyword-faces '(
 ("TODO" warning bold)
 ("FIXME" error bold)
 ("HACK" font-lock-constant-face bold)
 ("REVIEW" font-lock-keyword-face bold)
 ("DEPRECATED" font-lock-doc-face bold)
 ("NOTE" success bold)
 ("BUG" error bold)
 ("XXX" font-lock-constant-face bold)))



;; do not export todo in state in heading(ie: TODO, DONE)
(setq org-export-with-todo-keywords nil)
;;
;; highlight todo headlines in all files
(add-hook 'text-mode-hook 'hl-todo-mode )
;; styling for odt export
(with-eval-after-load 'org  (setq org-odt-styles-file "~/.config/emacs/stil-aflevering-style.ott"))

;; when opening files in orgmode(this is also true for export, but not for odt export, because that is buggy.)
(setq org-file-apps
    '(("\\.docx\\'" . "xdg-open \"%s\"")
      ;;("\\.mm\\'" . default)
      ("\\.x?html?\\'" . "xdg-open \"%s\"")
      ("\\.pdf\\'" . emacs)
      ("\\.odt\\'" . "xdg-open \"%s\"")
      ("\\.ods\\'" . "xdg-open \"%s\"")
      ("\\.ott\\'" . "xdg-open \"%s\"")
      ("\\.odf\\'" . "xdg-open \"%s\"")
      (auto-mode . emacs)))

;; on laptop the diff-hl-dired-mode-unless-remote does not work, so the hook
;; exits, meaning dired cannot start
 (setq dired-mode-hook
'(
  doom--recentf-add-dired-directory-h
 ;; dired-extra-startup
 +dired-disable-gnu-ls-flags-maybe-h
 ;; doom-modeline-set-project-modeline
 dired-omit-mode
 ;; all-the-icons-dired-mode
 ;; diff-hl-dired-mode-unless-remote
 diredfl-mode
display-line-numbers-mode  ;; show linenumbers in dired.
 ))

;; drag-and-drop in dired
(add-hook 'dired-mode-hook 'org-download-enable)

;; dired show directories/folders above files
(setq dired-listing-switches "-aBhl  --group-directories-first")

;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.



;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; orgmode pure settings ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;



;; import image file via file link instead of attachments via tagging, anki-editor does resolve attachments to file paths(ie attachments do not come with the output in anki)
;; directory links created do not work, so i'll stick to attachments, untill I am better at writing elisp
(setq org-download-screenshot-method "gnome-screenshot -a -f %s")

(after! org
  ;; (setq org-download-image-dir "./images/")
  ;;     (setq org-download-method 'directory)
;; i run Gnome as dwl does not have input method or drawing tablet support
;; (setq org-download-screenshot-method "gnome-screenshot -a -f %s")
;; this below is for gnome
;; (setq org-download-screenshot-method "gnome-screenshot -a -f %s")

;; (setq org-download-timestamp "%y%m%d-%h%m%s_")
(setq org-download-image-org-width 400)
)

;; make a local variable version, so completion not delayed like in orgmdoe
(add-hook 'org-mode-hook (lambda ()  (setq-local company-idle-delay 4)
	  		 (setq-local company-tooltip-idle-delay 4.2)
)			 )




;; when opening a org file, don't fold all the headers to their highest header
(eval-after-load 'org (setq org-startup-folded nil))

;; modified doom emacs templates
;; (setq org-capture-templates '(("t" "Personal todo" entry
;;   (file+headline +org-capture-todo-file "Inbox")
;;   "* TODO %?\n%i %a" :prepend t)
;;  ("n" "Personal notes" entry
;;   (file+headline +org-capture-notes-file "Inbox")
;;   "* %u %?\n%i\n%a" :prepend t)
;;  ("j" "Journal" entry
;;   (file+olp+datetree +org-capture-journal-file)
;;   "* %U %?\n%i\n%a" :prepend t)
;;  ("p" "Templates for projects")
;;  ("pt" "Project-local todo" entry
;;   (file+headline +org-capture-project-todo-file "Inbox")
;;   "* TODO %?\n%i\n%a" :prepend t)
;;  ("pn" "Project-local notes" entry
;;   (file+headline +org-capture-project-notes-file "Inbox")
;;   "* %U %?\n%i\n%a" :prepend t)
;;  ("pc" "Project-local changelog" entry
;;   (file+headline +org-capture-project-changelog-file "Unreleased")
;;   "* %U %?\n%i\n%a" :prepend t)
;;  ("o" "Centralized templates for projects")
;;  ("ot" "Project todo" entry #'+org-capture-central-project-todo-file "* TODO %?\n %i\n %a" :heading "Tasks" :prepend nil)
;;  ("on" "Project notes" entry #'+org-capture-central-project-notes-file "* %U %?\n %i\n %a" :heading "Notes" :prepend t)
;;  ("oc" "Project changelog" entry #'+org-capture-central-project-changelog-file "* %U %?\n %i\n %a" :heading "Changelog" :prepend t))
;;       )

;; return opens links in org-mode
(map!
 :map org-mode-map
:nv "<return>" #'+org/dwim-at-point)

;; navigaton between items
(map!
 :map org-mode-map

 :desc "jump to next item in list"      :nv "[ i" #'org-next-item
 :desc "jump to previous item in list"  :nv "] i" #'org-previous-item
)
;; insert todo items easily
(map!
 :map org-mode-map
  :desc "insert todo header" :ni "M-s-<return>" #'org-insert-todo-heading
  ;; the binding scheme does not make sense, but it's more ergonomic than C at capslog(this is done with the rebinding softw) and control or emacs pinky from normal control.
  :desc "insert todo subheader" :ni "M-s-j" #'org-insert-todo-subheading

  :desc "insert todo subheader" :ni "C-M-s-<return>" #'org-insert-todo-subheading

  ;; subtree
  :desc "paste subtree at current level" :ni "M-p" #'org-paste-subtree

  :desc "insert todo subheader" :ni "C-M-s-<return>" #'org-insert-todo-subheading
)


;; when the org file has a newline, then the output has a newline, send it as it looks
;; this is a godsend for latex export, if you have line length (fill-colum in emacs) to 71 or something like that.
;; else one line does not equal a latex 12pt line
(eval-after-load 'org (setq org-export-preserve-breaks 't))


;;;;;;;;;;;;;;;;;;;;;;;;;
;; anki, still org mode ;;
;;;;;;;;;;;;;;;;;;;;;;;;;


;; anki-mode bindings for making flashcards for the anki program
;; just disable if something,
(add-hook 'org-mode-hook (lambda () (anki-editor-mode t)
(setq anki-editor-ignored-org-tags '("export" "noexport" "ATTACH"))
			   ))

;; anki needs to run, for anki-editor-mode to work.
;; run anki when anki-editor is loaded, anki does not run more than 1 session
;; ;; do not use async-shell-command, it spawns a buffer, which we do not care for,
;; ;; we just want to start the application
;; (add-hook 'anki-editor-mode-hook (lambda)()
;; ;; no sandbox is needed because arch linux is officially unsupported
;; (call-process-shell-command "anki --no-sandbox &" nil 0)
;; )

;;
  ;; (setq-default anki-editor-use-math-jax t)

;; when using (toggle-input-method) this is the language to try to run, the default input method is not on by default,
(setq default-input-method "japanese")

;; better binding for input switching with a danish keybord
(map!
 ;; binding for: normal, insert, visual, emacs
 :nivre "C-x '" #'toggle-input-method
 :nivre "C-x RE C-'" #'toggle-input-method)

;; FEATURE is the name of the package.
(map! :after org
      :map org-mode-map
      ;; :desc "push anki notes" :nv "SPC i p" #'anki-editor-push-notes
)

;; (evil-define-key 'normal org-mode-map "SPC i p" 'anki-editor-push-notes)
;;
    (after! org (map!
     :after org
     :map org-mode-map
     ;; overwrites
     :n  "SPC i a" #'anki-editor-insert-note
     :desc "push anki notes"
     :nv "SPC i p" #'anki-editor-push-notes
     ;; :n  "SPC i p" #'anki-editor-push-notes
     ))

;; zenrough, by me
(defun zr/screenshot-and-edit ()
 "takes a screenshot and edit it with krita. "
 (interactive)
(org-download-screenshot)
;; execute the krita shell command on the absolute path for the cached file.
(shell-command-to-string
       (concat "gimp "  (expand-file-name  org-download-path-last-file)))
)
;; TODO Anki-editor-insert does not work non-interactively :(
;; because I do not understand enough raw format for lisp.
   (map!
    :after org
    :map org-mode-map
     :n "SPC i i" #'zr/screenshot-and-edit
     )

;; the suggested size when running (org-table-create)
(setq org-table-default-size "2x5")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; org-krita, also orgmode but used with Anki and roam ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(evil-define-key 'normal org-mode-map "SPC m a k" 'org-krita-insert-new-image)



 ;;;;;;;;;;;;;;;;;;;;;;;
 ;; roam, also orgmode ;;
 ;;;;;;;;;;;;;;;;;;;;;;;

;; the idea is to have some quick notes via org-roam
;; sync org-roam cache on startup
     ;; (org-roam-db-autosync-mode)

;; org roam templates, this allows you to be chaotic
;; %? is where text is inserted
;; (setq org-roam-capture-templates
;;       '(
;; 	("m" "main" plain
;;          :if-new (file+head "main/${slug}.org"
;;                             "#+title: ${title}\n")
;;          :immediate-finish t
;;          :unnarrowed t)

;; ;; 	;; TODO autotag file depending on first argument title, so something like
;; 	;; (save first argument of input file (file-name))
;;         ;; ("r" "reference" plain "* %? \n %a"
;;         ("r" "reference" plain "%? \n %a"
;;          :if-new
;;          (file+head "reference/${title}.org" "#+title: ${title}\n#+filetags: :reference:\n")
;;          :immediate-finish t
;; 	 ;; :prepend
;;          :unnarrowed t
;; 	 :empty-lines 1

;; 	 )

;;         ;; ("j" "japanese note" plain "%? \n %a"
;;         ;;  :if-new
;;         ;;  (file+head "reference/${title}.org" "#+title: ${title}\n#+filetags: :japanese:\n")
;;         ;;  :immediate-finish t
;; 	;;  ;; :prepend
;;         ;;  :unnarrowed t
;; 	;;  :empty-lines 1

;; ;; 	 )

;; ;; 	;; ("a" "article" plain "%?"
;; ;;         ;;  :if-new
;; ;;         ;;  (file+head "articles/${title}.org" "#+title: ${title}\n #+filetags: :article:\n")
;; ;;         ;;  :immediate-finish t
;; ;;         ;;  :unnarrowed t)

;; ))


;; when doing org roam capture, show the type(eg. reference, artivle or main) for different files.
;; (cl-defmethod org-roam-node-type ((node org-roam-node))
;;  "Return the TYPE of NODE."
;;   (condition-case nil
;;       (file-name-nondirectory
;;       (directory-file-name
;;        (file-name-directory
;;          (file-relative-name (org-roam-node-file node) org-roam-directory))))
;;     (error "")))
;; (setq org-roam-node-display-template
;;       ;; (concat "${type:15} ${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
;;       ;; (concat "${tags:10}" " ${title:*} " (propertize "${type:15}"'face )))
;;        (concat "${type:15}" " ${title:*} " (propertize "${tags:10}" 'face 'org-tag)))


;;;; some pdf mode maps, in Sat Dec 10 11:01:52 2022 conflicts with global modes ??
(map!
 :map pdf-view-mode-map
 ;; vim up and down without modifiers
  :ni "d" 'pdf-view-scroll-up-or-next-page
  :ni "u" 'pdf-view-scroll-down-or-previous-page
  ;; up down page
  :ni "J"   'pdf-view-next-page
  :ni "K"   'pdf-view-previous-page
  :ni "C-f" 'pdf-view-next-page
  :ni "C-b" 'pdf-view-previous-page
)


;; use the emacs theme for the pdf viewer also
;; toggling is done with "C-c C-r t"
(setq pdf-view-themed-minor-mode t)



;; ;; line spacing for easier proofreading
;; (add-hook 'org-mode-hook (lambda ()(setq line-spacing 0.2)))

;; stick to 8 char tabs and indents, linux kernel style
;; https://www.kernel.org/doc/html/v4.10/process/coding-style.html
(setq-default c-basic-offset 8)



;; general settings
(setq-default indent-tabs-mode t)

;; colored parenthesis according to matching in programming modes, is never a bad thing
(add-hook 'prog-mode-hook (rainbow-delimiters-mode 1))
 (add-hook 'org-mode-hook (rainbow-delimiters-mode 1))
 (add-hook 'tex-mode-hook (rainbow-delimiters-mode 1))

;; let ran code snipppets do nothing in 1 min
(setq quickrun-timeout-seconds '60)
;; different priority of tex viewers
;; pdf-tools is the emacs extension pdf viewer, it is prioritized first.
(setq +latex-viewers '(pdf-tools zathura skim evince sumatrapdf okular ))

(defun latex-compile-and-update-other-buffer ()
    "Has as a premise that it's run from a latex-mode buffer and the
     other buffer already has the PDF open"
    (interactive)
    (save-buffer)
    (shell-command (concat "pdflatex " (buffer-file-name)))
    (switch-to-buffer (other-buffer))
    (kill-buffer)
    (update-other-buffer))

;; use xelatex as compiler for export, as pdflatex as xetex has full utf8 support(that means support for japanese kanji)
(setq-default org-latex-compiler "xelatex")
;; (setq-default org-latex-compiler "pdflatex")
;; (setq-default org-latex-compiler "xelatex")
;; TeX-command-list is only initialised when texmode is entered, so we need to run the function after the mode is loaded
;; (with-eval-after-load 'tex (add-to-list 'TeX-command-list '("XeLaTeX" "%`xelatex --synctex=1%(mode)%' %t" TeX-run-TeX nil t)))

;; show agenda on startup
  ;; (add-hook 'after-init-hook (lambda () (org-agenda nil "a")))

;; i do not like langtool for danish, it does not check grammar, it only checks spacing og comma.
;; apparently only langtool 5.8 is working, but when latest is compiled it did not work
;; https://languagetool.org/download/
;;
;; This is the fallback value when no language is recognized
;; no recognition happens everytime with danish so set this if you are running langtool on a non-english document.
;; (setq langtool-default-language "da-DK")
;; (setq langtool-mother-tongue "da-DK")
;; (setq langtool-bin "/usr/bin/languagetool")
;; (setq langtool-language-tool-jar "/usr/bin/languagetool")
;; (setq langtool-java-classpath
;;       "/usr/share/languagetool:/usr/share/java/languagetool/*")
;;
;; doom and use-package loads the things automaticly
;; (require 'langtool)
;; (setq languagetool-java-arguments '("-Dfile.encoding=UTF-8"))
;; https://github.com/PillFall/languagetool.el
;; force usage of console or cli interface
;; (setq languagetool-console-command "org.languagetool.commandline.Main")
;; (add-hook 'org-mode-hook
;;            (lambda ()
;;               (add-hook 'after-save-hook 'langtool-check nil 'make-it-local)))
;; force combined binary to use cli interface instead of starting gui and cli
;; (setq langtool-user-arguments '("interface cli"))


;; ediff combine diffs always returns nil, this works supposedly by combining without the version control indicators.
(defun ediff-copy-both-to-C ()
  (interactive)
  (ediff-copy-diff ediff-current-difference nil 'C nil
                   (concat
                    (ediff-get-region-contents ediff-current-difference 'A ediff-control-buffer)
                    (ediff-get-region-contents ediff-current-difference 'B ediff-control-buffer))))
;; add keymap
(defun add-d-to-ediff-mode-map () (map! :map ediff-mode-map :ne "d" 'ediff-copy-both-to-C))
(add-hook 'ediff-keymap-setup-hook 'add-d-to-ediff-mode-map)



;; TODO how to open todo file shortcut,
;; nvm there is already default/org-notes-search
;; (+default/find-in-notes)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; calendar, rss, and mail ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; load file for private credentials stuff: calendar synching and so on.
;; (load-file "./private-config.el")

;; Mon Aug  8  2022 damn org-gcal is still broken.
;; remember to hold  org-gcal-client-id and org-gcal-client-secret in a seperate file,
;; as these are what are username and password to access your calendar.
;; this is what is in the file
;; require 'org-gcal)
;; (setq org-gcal-client-id "your-id-foo.apps.googleusercontent.com"
;;       org-gcal-client-secret "your-secret"
;;       org-gcal-fetch-file-alist '(("your-mail@gmail.com" .  "~/schedule.org")
;;                                   ("another-mail@gmail.com" .  "~/task.org")))
;;

(setq-default tab-width 8) ; Assuming you want your tabs to be four spaces wide

;; impatient-mode on git markdown files
  (defun markdown-html (buffer)
    (princ (with-current-buffer buffer
      (format "<!DOCTYPE html><html><title>Impatient Markdown</title><xmp theme=\"united\" style=\"display:none;\"> %s  </xmp><script src=\"http://strapdownjs.com/v/0.2/strapdown.js\"></script></html>" (buffer-substring-no-properties (point-min) (point-max))))
    (current-buffer)))

;; Use danish calendar names and holydays Copyright (c) 2018 Søren Lund <soren@lund.org>
(add-to-list 'load-path "~/.config/emacs/elisp/")
;; (load "da-kalender")


;;;;;;;;;;;;;;;;;
;; font things ;;
;;;;;;;;;;;;;;;;;


;; height/spacing between lines in the editor.
;; (setq line-spacing 0.5)



;; set how big the default font is, default size is 100
;; inconsolata is in the ubuntu repositories.
;; (set-frame-font "Inconsolata 14" nil t)
(set-face-attribute 'default nil :height 120)


;; the size of inline images while viewed in org-mode
(setq org-image-actual-width 400)

;; the size of the emacs headers compared to default font
(custom-set-faces
  '(org-level-1 ((t (:inherit outline-1 :height 2.0))))
  '(org-level-2 ((t (:inherit outline-2 :height 1.4))))
  '(org-level-3 ((t (:inherit outline-3 :height 1.2))))
  '(org-level-4 ((t (:inherit outline-4 :height 1.0))))
  ;; last line is smaller to visually indicate that, you are too deep in subsections.
  '(org-level-5 ((t (:inherit outline-5 :height 0.8))))
)
;; use arrow to signal that a heading is folded
(setq org-ellipsis "⤵")
;; maybe change the font if you don't like the icon, I don't care as long as it
;; is eyecatching.
;;
;; needed for rendering arrow always, because of the way orgmode is implemented.
(setq org-cycle-separator-lines -1)


;;
;; autoinserts newlines when wordcount is above the value of variable fill-column
(add-hook 'org-mode-hook (lambda () '((auto-fill-mode t)(setq fill-column 71)) ))

;; let marked ephasis regions be 10 lines long.
(setq org-emphasis-regexp-components '("-[:space:]('\"{" "-[:space:].,:!?;'\")}\\[" "[:space:]" "." 10))
;; (org-set-emph-re 'org-emphasis-regexp-components org-emphasis-regexp-components)




;; display date and time in emacs, then I can go fullscreen without loosing sense of time.
;; (setq display-time-day-and-date t)
   ;; display-time-24)

;; TODO display battery, only when on device with battery.
;; ( display-battery-mode)

;; open documents in nov-mode for epub documents
(add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))

;; the lower number means a higher priority, a good way to think of it is as a race, number one first, second, third
;; by default only 3 values are used, 65,66,67
(setq org-priority-default 67)

;; doom emacs loads automatically package
;; (after! org )
;; (require 'guess-language)

;; for all buffers, use danish dictionary by default.
(setq-default ispell-dictionary "dansk")

;; ;; straight ispell is supposed to adhere to the value of ispell-dictionary, but straight up ignores it and says, yes of course everything is bonjour francoise.
;; (add-hook 'org-mode-hook (lambda () #'(setq ispell-current-dictionary "danish")))
;; ;; choose in the ispell-dictionary-alist, which dictionary we should use, this works on linux, in WSL on windows 10 though.. NO It says french...
;; ;
;; ;; ;; Optionally a package for multilingual writing.:
;; ;; choose in the ispell-dictionary-alist, which dictionary we should use, this works on linux, in WSL on windows NO It says french...
;; ;; (setq-default ispell-local-dictionary "da")
;; ;; ;; Optionally:
;; (add-hook 'guess-language-mode-hook '
;; (setq guess-language-languages '(en da))
;; (setq guess-language-min-paragraph-length 35)

;; (setq guess-language-langcodes
;;   '((en . ("en_US" "English" "🇺🇸" "US English"))
;;     ;; (de . ("de_CH" "German" "🇨🇭" "Swiss German"))))
;; )))

;; for when executing extra long lines
;; (setq max-lisp-eval-depth 12000)
;; not a doom package, but it allows changing language according to paragraph automatically
;; (add-hook 'org-mode-hook 'guess-language-mode)
(add-hook 'latex-mode-hook
(lambda () '(
;; (guess-language-mode t)
(setq fill-column 70)
(auto-fill-mode 1)
 (display-fill-column-indicator-mode 1)
)))

;; idk, there was a weird bug earlier that this is needed to fix.
(advice-remove 'org--flyspell-object-check-p 'org-ref-cite--flyspell-object-check-p)

;; display eshell as pop up
;; (add-to-list 'display-buffer-alist
;;              '("\\`\\*eshell\\*\\(?:<[[:digit:]]+>\\)?\\'"
;;                (display-buffer-in-side-window (side . bottom)))
;; )

;; The maximum of subheadings shown in Table Of Contents, if depth was 1 then
;; TOC would only show the 1. headings/titles.
(with-eval-after-load 'org (setq toc-org-max-depth 4))

(add-hook 'elfeed-show-mode-hook (lambda()
(display-line-numbers-mode 'relative)
))

;; For full emacs dwelling add time, also displays the day according to caledendar.
;; Very usefull if you don't have lots of screenspace like a laptop.
(add-hook 'text-mode-hook 'display-time-mode)
(setq display-time-24hr-format t)
;; do not show system average.
(setq display-time-default-load-average nil)

;; can't change the default face on vanilla version of the function, as it is inherited.
;;
;; show which lines are folded/continued in visual-line-mode
(setq visual-line-fringe-indicators '(left-curly-arrow right-curly-arrow))

;; Happily taken from Pascals configuration: https://github.com/SirPscl/emacs.d#sudo-save
(defun ph/sudo-file-name (filename)
  "Prepend '/sudo:root@`system-name`:' to FILENAME if appropriate.
If the file already has a tramp prefix, return nil."
  (when (and filename
             (not (file-remote-p filename)))
    (format "/sudo:root@%s:%s" (system-name) filename)))

(defun ph/sudo-save-buffer ()
  "Save buffer as root if the user approves."
  (let ((filename (ph/sudo-file-name (buffer-file-name))))
    (when (and filename
               (yes-or-no-p (format "Save file as %s ? " filename)))
      (write-file filename))))

(advice-add 'save-buffer :around
            #'(lambda (fn &rest args)
               (when (or (not (buffer-file-name))
                         (not (buffer-modified-p))
                         (file-writable-p (buffer-file-name))
                         (not (ph/sudo-save-buffer)))
                 (call-interactively fn args))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; window management and buffers ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; TODO display-buffer-alist

;; Value
;; (("^\\*TeX \\(?:Help\\|errors\\)"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . 0.3)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot)
;;   (vslot)
;;   (window-parameters
;;    (ttl)
;;    (quit . t)
;;    (select . t)
;;    (modeline)
;;    (autosave)))
;;  (" output\\*$"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . 15)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot)
;;   (vslot)
;;   (window-parameters
;;    (ttl . 5)
;;    (quit . t)
;;    (select . ignore)
;;    (modeline)
;;    (autosave)))
;;  ("^\\*Flycheck errors\\*"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . 0.25)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot)
;;   (vslot)
;;   (window-parameters
;;    (ttl . 5)
;;    (quit . t)
;;    (select . ignore)
;;    (modeline)
;;    (autosave)))
;;  ("^\\*Flycheck error messages\\*"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot)
;;   (vslot)
;;   (window-parameters
;;    (ttl . 5)
;;    (quit . t)
;;    (select)
;;    (modeline)
;;    (autosave)))
;;  ("^\\*Ibuffer\\*$" nil)
;;  ("^\\(?:\\*magit\\|magit:\\| \\*transient\\*\\)" nil)
;;  ("^\\(?:\\*magit\\|magit:\\| \\*transient\\*\\)" nil)
;;  ("^\\*image-dired"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . 0.8)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot . 20)
;;   (vslot)
;;   (window-parameters
;;    (ttl . 0)
;;    (quit)
;;    (select . t)
;;    (modeline)
;;    (autosave)))
;;  ("^\\*org-roam: "
;;   (+popup-buffer)
;;   (actions)
;;   (side . right)
;;   (size)
;;   (window-width . 0.33)
;;   (window-height . 0.5)
;;   (slot . 2)
;;   (vslot)
;;   (window-parameters
;;    (ttl)
;;    (quit)
;;    (select . ignore)
;;    (modeline)
;;    (autosave)))
;;  ("\\*org-roam\\*"
;;   (+popup-buffer)
;;   (actions)
;;   (side . right)
;;   (size)
;;   (window-width . 0.33)
;;   (window-height . 0.5)
;;   (slot . 1)
;;   (vslot)
;;   (window-parameters
;;    (ttl)
;;    (quit)
;;    (select . ignore)
;;    (modeline)
;;    (autosave)))
;;  ("^\\*org-roam: "
;;   (+popup-buffer)
;;   (actions)
;;   (side . right)
;;   (size)
;;   (window-width . 0.33)
;;   (window-height . 0.5)
;;   (slot . 2)
;;   (vslot)
;;   (window-parameters
;;    (ttl)
;;    (quit)
;;    (select . ignore)
;;    (modeline)
;;    (autosave)))
;;  ("\\*org-roam\\*"
;;   (+popup-buffer)
;;   (actions)
;;   (side . right)
;;   (size)
;;   (window-width . 0.33)
;;   (window-height . 0.5)
;;   (slot . 1)
;;   (vslot)
;;   (window-parameters
;;    (ttl)
;;    (quit)
;;    (select . ignore)
;;    (modeline)
;;    (autosave)))
;;  ("^\\*Capture\\*$\\|CAPTURE-.*$"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . 0.42)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot)
;;   (vslot)
;;   (window-parameters
;;    (ttl . 5)
;;    (quit)
;;    (select . t)
;;    (modeline)
;;    (autosave . ignore)))
;;  ("^\\*Org-Babel"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot)
;;   (vslot)
;;   (window-parameters
;;    (ttl . 5)
;;    (quit . t)
;;    (select . ignore)
;;    (modeline)
;;    (autosave)))
;;  ("^\\*Org Src"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . 0.42)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot)
;;   (vslot)
;;   (window-parameters
;;    (ttl)
;;    (quit)
;;    (select . t)
;;    (modeline . t)
;;    (autosave . t)))
;;  ("^\\*Org Agenda" nil)
;;  ("^\\*Org \\(?:Select\\|Attach\\)"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . 0.25)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot . -1)
;;   (vslot . -2)
;;   (window-parameters
;;    (ttl . 0)
;;    (quit . t)
;;    (select . ignore)
;;    (modeline)
;;    (autosave)))
;;  ("^ ?\\*\\(?:Agenda Com\\|Calendar\\|Org Export Dispatcher\\)"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size function +popup-shrink-to-fit)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot . -1)
;;   (vslot . -1)
;;   (window-parameters
;;    (ttl . 0)
;;    (quit . t)
;;    (select . ignore)
;;    (modeline)
;;    (autosave)))
;;  ("^\\*Org Links"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . 2)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot . -1)
;;   (vslot . -1)
;;   (window-parameters
;;    (ttl . 0)
;;    (quit . t)
;;    (select . ignore)
;;    (modeline)
;;    (autosave)))
;;  ("^\\*Command Line"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . 8)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot)
;;   (vslot)
;;   (window-parameters
;;    (ttl . 5)
;;    (quit . t)
;;    (select . ignore)
;;    (modeline)
;;    (autosave)))
;;  ("^\\*evil-registers"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . 0.3)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot)
;;   (vslot)
;;   (window-parameters
;;    (ttl . 5)
;;    (quit . t)
;;    (select . ignore)
;;    (modeline)
;;    (autosave)))
;;  ((closure
;;    (t)
;;    (bufname _)
;;    (if
;;        (boundp '+eval-repl-mode)
;;        (progn
;; 	 (buffer-local-value '+eval-repl-mode
;; 			     (get-buffer bufname)))))
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . 0.25)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot)
;;   (vslot)
;;   (window-parameters
;;    (ttl closure
;; 	(t)
;; 	(buf)
;; 	(if
;; 	    (plist-get +eval-repl-plist :persist)
;; 	    nil
;; 	  (let*
;; 	      ((process
;; 		(and t
;; 		     (get-buffer-process buf))))
;; 	    (if process
;; 		(progn
;; 		  (set-process-query-on-exit-flag process nil)
;; 		  (kill-process process)
;; 		  (kill-buffer buf))
;; 	      nil))))
;;    (quit)
;;    (select . ignore)
;;    (modeline)
;;    (autosave)))
;;  ("^\\*\\(?:Proced\\|timer-list\\|Abbrevs\\|Output\\|Occur\\|unsent mail.*?\\|message\\)\\*" nil)
;;  ("^\\*Process List\\*"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . 0.25)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot)
;;   (vslot . 101)
;;   (window-parameters
;;    (ttl . 5)
;;    (quit . t)
;;    (select . t)
;;    (modeline)
;;    (autosave)))
;;  ("^\\*Memory-Profiler-Report "
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size)
;;   (window-width . 0.5)
;;   (window-height . 0.4)
;;   (slot . 2)
;;   (vslot . 100)
;;   (window-parameters
;;    (ttl . 5)
;;    (quit)
;;    (select . ignore)
;;    (modeline)
;;    (autosave)))
;;  ("^\\*CPU-Profiler-Report "
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size)
;;   (window-width . 0.5)
;;   (window-height . 0.4)
;;   (slot . 1)
;;   (vslot . 100)
;;   (window-parameters
;;    (ttl . 5)
;;    (quit)
;;    (select . ignore)
;;    (modeline)
;;    (autosave)))
;;  ("^\\*Backtrace"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . 0.4)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot)
;;   (vslot . 99)
;;   (window-parameters
;;    (ttl . 5)
;;    (quit)
;;    (select . ignore)
;;    (modeline)
;;    (autosave)))
;;  ("^\\*Warnings"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . 0.25)
;;   (window-width . 40)
;;   (window-height . 0.25)
;;   (slot)
;;   (vslot . 99)
;;   (window-parameters
;;    (ttl . 5)
;;    (quit . t)
;;    (select . ignore)
;;    (modeline)
;;    (autosave)
;;    (transient . t)
;;    (no-other-window . t)))
;;  ("^\\*info\\*$"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . 0.45)
;;   (window-width . 40)
;;   (window-height . 0.45)
;;   (slot . 2)
;;   (vslot . 2)
;;   (window-parameters
;;    (ttl . 5)
;;    (quit . t)
;;    (select . t)
;;    (modeline)
;;    (autosave)
;;    (transient . t)
;;    (no-other-window . t)))
;;  ("^\\*xwidget"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . 0.35)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot)
;;   (vslot . -11)
;;   (window-parameters
;;    (ttl . 5)
;;    (quit . t)
;;    (select)
;;    (modeline)
;;    (autosave)))
;;  ("^\\*eww\\*"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . 0.35)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot)
;;   (vslot . -11)
;;   (window-parameters
;;    (ttl . 5)
;;    (quit . t)
;;    (select . t)
;;    (modeline)
;;    (autosave)))
;;  ("^\\*\\([Hh]elp\\|Apropos\\)"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . 0.42)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot . 2)
;;   (vslot . -8)
;;   (window-parameters
;;    (ttl . 5)
;;    (quit . t)
;;    (select . t)
;;    (modeline)
;;    (autosave)))
;;  ("^ \\*undo-tree\\*"
;;   (+popup-buffer)
;;   (actions)
;;   (side . left)
;;   (size . 20)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot . 2)
;;   (vslot)
;;   (window-parameters
;;    (ttl . 5)
;;    (quit . t)
;;    (select . t)
;;    (modeline)
;;    (autosave)))
;;  ("^\\*Customize"
;;   (+popup-buffer)
;;   (actions)
;;   (side . right)
;;   (size . 0.5)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot . 2)
;;   (vslot)
;;   (window-parameters
;;    (ttl . 5)
;;    (quit)
;;    (select . t)
;;    (modeline)
;;    (autosave)))
;;  ("^\\*Calc"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . 0.4)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot)
;;   (vslot . -7)
;;   (window-parameters
;;    (ttl . 0)
;;    (quit)
;;    (select . t)
;;    (modeline)
;;    (autosave)))
;;  ("^\\*\\(?:Wo\\)?Man "
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . 0.45)
;;   (window-width . 40)
;;   (window-height . 0.45)
;;   (slot)
;;   (vslot . -6)
;;   (window-parameters
;;    (ttl . 0)
;;    (quit . t)
;;    (select . t)
;;    (modeline)
;;    (autosave)
;;    (transient . t)
;;    (no-other-window . t)))
;;  ("^\\*doom:\\(?:v?term\\|e?shell\\)-popup"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . 0.35)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot)
;;   (vslot . -5)
;;   (window-parameters
;;    (ttl)
;;    (quit)
;;    (select . t)
;;    (modeline)
;;    (autosave)))
;;  ("^\\*doom:"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . 0.35)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot)
;;   (vslot . -4)
;;   (window-parameters
;;    (ttl . t)
;;    (quit)
;;    (select . t)
;;    (modeline . t)
;;    (autosave . t)))
;;  ("^\\*\\(?:doom \\|Pp E\\)"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . +popup-shrink-to-fit)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot)
;;   (vslot . -3)
;;   (window-parameters
;;    (ttl . 0)
;;    (quit . t)
;;    (select . ignore)
;;    (modeline)
;;    (autosave . t)))
;;  ("^\\*\\(?:[Cc]ompil\\(?:ation\\|e-Log\\)\\|Messages\\)"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . 0.3)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot)
;;   (vslot . -2)
;;   (window-parameters
;;    (ttl)
;;    (quit . t)
;;    (select . ignore)
;;    (modeline)
;;    (autosave . t)))
;;  ("^\\*Local variables\\*$"
;;   (+popup-buffer)
;;   (actions)
;;   (side . bottom)
;;   (size . +popup-shrink-to-fit)
;;   (window-width . 40)
;;   (window-height . 0.16)
;;   (slot . 1)
;;   (vslot . -1)
;;   (window-parameters
;;    (ttl . 5)
;;    (quit . t)
;;    (select . ignore)
;;    (modeline)
;;    (autosave)))
;;

;; adding lookup options for japanese dictionaries.
(cl-pushnew '("ji jisho general words" "https://jisho.org/search/%s") +lookup-provider-url-alist )
(cl-pushnew '("jik jisho kanji" "https://jisho.org/search/%23kanji%s") +lookup-provider-url-alist)
;; japandict has pitch accent, so it is prefereable, but jisho has more slang.
(cl-pushnew '("j japandict " "https://www.japandict.com/?s=%s&lang=eng") +lookup-provider-url-alist)
(cl-pushnew '("jk japandict " "https://www.japandict.com/kanji/?s=%s&lang=eng&list=1") +lookup-provider-url-alist)
(cl-pushnew '("java documentation " "https://docs.oracle.com/search/?category=java&q=%s") +lookup-provider-url-alist)
;; lav limit til site java, og brug bare duck duck go. TODO



;; NYE TING, new things
;; if running winows on wsl.
;; (setq org-krita-executable "powershell.exe -c StartProcess -FilePath /mnt/c/Program Files/Krita (x64)/bin/krita.exe")
;; (setq org-krita-executable "powershell.exe -c StartProcess -FilePath 'C:\Program Files\Krita (x64)/bin\krita.exe'")


;; I use latex-environment to math fragments, functions definition is void
(setq LaTeX-default-environment "math")


;; TODO org-krita bigger template, modify this variable.
;; org-krita-resource

;; If this is run, then it does not show this error "Error running timer ‘flyspell-lazy-check-pending’: (error "Undefined dictionary: de") [2 times]"
;; but it does not fix it, it's something with org-cite.
(advice-remove 'org--flyspell-object-check-p 'org-ref-cite--flyspell-object-check-p)

;; get size of selected file in dired.
(defun dired-get-size ()
  (interactive)
  (let ((files (dired-get-marked-files)))
    (with-temp-buffer
      (apply 'call-process "/usr/bin/du" nil t nil "-sch" files)
      (message "Size of all marked files: %s"
               (progn
                 (re-search-backward "\\(^[0-9.,]+[A-Za-z]+\\).*total$")
                  (match-string 1))))))

 ;; (define-key dired-mode-map (kbd "?") 'dired-get-size)

;; key hey, that is the anki latex environment
(add-hook 'org-mode-hook (lambda)() #'(add-to-list 'cdlatex-env-alist
 '("equation*" "\\begin{equation*}\nAUTOLABEL\n?\n\\end{equation*}" nil)
))

;; DONE scale of org-latex-preview to double of normal
(add-hook 'org-mode-hook (lambda) () #'(setq org-format-latex-options (plist-put org-format-latex-options :scale 2.0)))

;; setup emacs org-mode sourceblocks for java
(add-hook 'org-mode-hook (lambda)() #'(
	  (org-babel-do-load-languages
	'org-babel-load-languages
	'(
	  (java . t) ;; load java
	  (C . t) ;; load c language
	  (python . t) ;; load python
	  (R . t) ;; R the statistics language
	  ))

(nconc org-babel-default-header-args:java
       '((:dir . nil)
         (:results . value)))
))

;; for exams disable java completion
;; (add-hook 'java-mode-hook '(setq company-mode nil))


;; sloppy focus for emacs windows, this is built in, active window is where the mouse is.
(setq mouse-autoselect-window t)

;; DO NOT suggest completion of words in buffer, I want my completion suggestions to be for structure or other things.
;; seams this does not work to remove
;; (add-hook 'org-mode-hook  '(company-dabbrev nil))
;; (add-hook 'latex-mode-hook  '(company-dabbrev nil))

;; (add-hook 'org-mode-hook  '(company-dict nil))

;; Don't use company-dabbrev as company-backend, as that tries to complete using words in buffer, which makes completion incosistent
;; this is annoying as ... as I have to wait for the completion candidates to show up and then choose the correct one.
;;  (add-hook 'org-mode-hook (lambda () #'(company-capf (:separate company-yasnippet))))
;;  (add-hook 'latex-mode-hook (lambda () #'(
;;   (setq 'company-backends '(company-capf (:separate company-yasnippet))
;; )) ))
;; set gf in orgmoed to org-emphasize
;; nvm can just use i just give up

;; if running kde
;; (setq org-download-screenshot-method "spectacle")
;; (add-hook 'org-mode-hook (lambda () #'(setq org-download-screenshot-method "grim -g \"$(slurp)\" %s")))
;; (setq org-download-screenshot-method "grim -g \"$(slurp)\" %s")
;; (setq org-download-screenshot-method "grimshot copy area")

;; show the agenda as first liste
(add-hook 'after-init-hook 'org-agenda-list)

;; header org coding src blocks generic settings
(after! org (set 'org-babel-default-header-args
'((:session . "none")
 (:results . "replace")
 (:exports . "code")
 (:cache . "yes")
 (:noweb . "no")
 (:hlines . "no")
 ;; tangle with links
 (:tangle . "both")))
)

;; ;; where my attached documents and pictures go
;; ;; (setq org-attach-id-dir (concat org-directory "/.attach"))
;; ;; move files, as attachments are typically ~/Downloads
;; (setq org-attach-method (quote mv))
;; ;; if no dir use this
;; (setq org-attach-preferred-new-method (quote dir))
;; ;; use this
;; (setq org-attach-preferred-new-method (quote directory))
;; ;; always store link path as relative
;; (setq org-link-file-path-type 'relative )
;; (setq org-attach-store-link-p 'file)


;; (defun yourname/create_temp_dir ()
;; (setq uuid  (org-id-copy)) (setq dir_temp (org-attach-id-uuid-folder-format uuid)) (setq dir_temp2 (concat org-attach-directory "/" dir_temp "/"))
;; (setq dir_temp_usage (concat org-attach-directory "/" "TEMP/")) (org-set-property "DIR" dir_temp_usage) (copy-directory dir_temp2 dir_temp_usage t t t))
;; (global-set-key (kbd "<f12>") (lambda () (interactive) (yourname/create_temp_dir)))

(after! org-pomodoro
  (setq org-pomodoro-format "Pom %s")
)
