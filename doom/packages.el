;; -*- no-byte-compile: t; -*-
;;; $DOOMDIR/packages.el
;;;
;;; this is the file choosing packages with no-doom support

;; To install a package with Doom you must declare them here and run 'doom sync'
;; on the command line, then restart Emacs for the changes to take effect -- or
;; use 'M-x doom/reload'.


;; To install SOME-PACKAGE from MELPA, ELPA or emacsmirror:
;(package! some-package)

;; To install a package directly from a remote git repo, you must specify a
;; `:recipe'. You'll find documentation on what `:recipe' accepts here:
;; https://github.com/raxod502/straight.el#the-recipe-format
;(package! another-package
;  :recipe (:host github :repo "username/repo"))

;; If the package you are trying to install does not contain a PACKAGENAME.el
;; file, or is located in a subdirectory of the repo, you'll need to specify
;; `:files' in the `:recipe':
;(package! this-package
;  :recipe (:host github :repo "username/repo"
;           :files ("some-file.el" "src/lisp/*.el")))

;; If you'd like to disable a package included with Doom, you can do so here
;; with the `:disable' property:
;(package! builtin-package :disable t)

;; You can override the recipe of a built in package without having to specify
;; all the properties for `:recipe'. These will inherit the rest of its recipe
;; from Doom or MELPA/ELPA/Emacsmirror:
;(package! builtin-package :recipe (:nonrecursive t))
;(package! builtin-package-2 :recipe (:repo "myfork/package"))

;; Specify a `:branch' to install a package from a particular branch or tag.
;; This is required for some packages whose default branch isn't 'master' (which
;; our package manager can't deal with; see raxod502/straight.el#279)
;(package! builtin-package :recipe (:branch "develop"))

;; Use `:pin' to specify a particular commit to install.
;(package! builtin-package :pin "1a2b3c4d5e")


;; Doom's packages are pinned to a specific commit and updated from release to
;; release. The `unpin!' macro allows you to unpin single packages...
;(unpin! pinned-package)
;; ...or multiple packages
;(unpin! pinned-package another-pinned-package)
;; ...Or *all* packages (NOT RECOMMENDED; will likely break things)
;(unpin! t)


;; coloring is enough to assertain heading level after some time using org bullets with the same colorscheme.
;; (package! org-bullets :recipe
;;  (:host github
;;   :repo "sabof/org-bullets"
;;   :files ("*.el" "src/lisp/*.el")))


;; install kbd-mode from github
;; kbd-mode is a major mode for kmonad files,
;; this is just lisp with other functions. so you can choose to not use the pacakge
;; (package! kbd-mode :recipe
;;   (:host github
;;    :repo "kmonad/kbd-mode"
;;    :files ("*.el" "src/lisp/*.el")))



;; package neaded for org-roam, if you like that
;; (package! sqlite3)

;; minor mode, for making anki flashcards in emacs
 (package! anki-editor)


;; use org-present instead of org-reveal, because web browsers are to be avoided
 ;; as they are unergonic, non-extensible, ressource intensive and generally a slow experience
(package! org-present)


;; org-notifications notify of org todos as desktop notifications.

;; impatient reload html files for you automatically when files are changed.
;; this is in the web module.

;; major mode for reading epub.
(package! nov)

;; spaces just enough
;; (package! smart-tabs-mode)

;; change flyspell dictionary to the one used in a paragraph
(package! guess-language)

;; make krita
(package! org-krita
  :recipe (:host github
           :repo "lepisma/org-krita"
           :files ("resources" "resources" "*.el" "*.el")))
