(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auth-source-save-behavior nil)
 '(elfeed-feeds
   '("http://www.dr.dk/nyheder/service/feeds/vejret" "http://www.gamersnexus.net/news/feed" "http://www.information.dk/feed" "https://console.substack.com/feed" "https://rss.dw.com/xml/rss-de-eco" "https://rsshub.app/github/issue/headllines/hackernews-daily" "https://www.reddit.com/r/NHKEasyNews/.rss"))
 '(magit-todos-insert-after '(bottom) nil nil "Changed by setter of obsolete option `magit-todos-insert-at'")
 '(org-agenda-files (list org-directory))
 '(org-directory "~/Documents/org")
 '(org-download-screenshot-method "grim -g \"$(slurp)\" %s")
 '(package-selected-packages '(@ use-package))
 '(warning-suppress-types '((org-element-cache))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-level-1 ((t (:inherit outline-1 :height 2.0))))
 '(org-level-2 ((t (:inherit outline-2 :height 1.4))))
 '(org-level-3 ((t (:inherit outline-3 :height 1.2))))
 '(org-level-4 ((t (:inherit outline-4 :height 1.0))))
 '(org-level-5 ((t (:inherit outline-5 :height 0.8)))))
