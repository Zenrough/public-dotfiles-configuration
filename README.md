
# Dotfiles
Dotfiles for window managers/compositors, and utilities

# Programs
- Editor      : Primarily Doom Emacs for writing and coding, then secondly neovim for editing config files.

- web browser : Primarily Firefox, then either brave or chromium(I use vimium for navigation)

- launcher    : Rofi X11 or Yofi for wayland

- wayland wm  : Dwl (dwl for wayland, which is modern but does not support drawing tablet) or sway

- Xorg wm(main): Awesomewm(tag based like dwl but modern it also supports drawing tablets)

- Music 	  : Cmus (terminal music player)

- Key rebinder: Kmonad(should be compatible with windows, macOS and linux, It allows you to run commands from keybindings no matter the graphical enviroment or windowmanager.)

- file manager: Thunar

## About my choice of programs
I like programs and utilities that are self contained, this is programs which have few dependencies on non-standard or platform specific features.
I also like programs which use keybindings and allow for automation, automation via scripts or macros and saving settings via config files.

## other software
This section is included because I find it annoying, to have to discover what 
that software is, just to be dissapointed that software is a note taking app, 
when you have emacs roam and neovim.

i3 is a window tiler with no tag support and h and l to move windows, which
makes it suboptimal for me. Gammastep is the redshift fork on wayland, because
redshift was slow to intergrate wayland support. 

River, is a wayland compositor writtin in Zig which is cool. You interface with
it with bash. It just feels off, for me, even though it is a fine compositor
according to my tastes. 

Zathura is a pdf viewer, which is probably loved by vim users. Sadly it has some pointed edges that
Pdf-mode in Emacs is better at than Zathura:
- rebinding keys(this the important one)
- index 

htop is probably already known, it is a process manager like "task manager" in
windows. But it is a lot better, you can search for program names, see with
processes depend on a high level process. 
An example is that everything is a subprocess of "init" which is the process
that starts the system OS.












## License
Added to remove confusion

Copyright <2022> <Mathias Aagaard Røjleskov>

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
