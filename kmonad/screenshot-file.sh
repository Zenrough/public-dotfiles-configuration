#!/usr/bin/env bash
# programs needed are:
# grim + slurp for wayland
# scrot            for X11

# for copying to clipboard you need:
# wl-clipboard
# xsel
mkdir -p ~/Pictures/screenshots # ensures that the directory exists
# wayland" (getenv "XDG_SESSION_TYPE"))
# wayland

FILENAME="screenshot-$(date +%F-%T)"
if [ "x11" == "$XDG_SESSION_TYPE" ]; then

 scrot -s $HOME/Pictures/screenshots/$FILENAME.png


elif [[ "wayland" == "$XDG_SESSION_TYPE" ]]; then

grim -g "$(slurp)" ~/Pictures/screenshots/$FILENAME.png

fi

# Xorg/X11

# scrot -s $HOME/Pictures/screenshots/'date +%Y-%d-%m_%H:%M:%S'.png
