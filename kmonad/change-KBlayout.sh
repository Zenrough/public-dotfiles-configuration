#!/usr/bin/env sh

# switch language if matching, you have to make sure it is the right layout name first.
l1="dk"
l2="us"

#
KB=$(setxkbmap -query | grep layout | awk '{print $2}')
# echo "previously $KB"


if [ "$KB" = "$l2" ]; then
     setxkbmap $l1
     echo "set to $l1"

elif [ "$KB" = "$l1" ];then
   setxkbmap $l2
     # echo "set to $l2"

# just move fi to the lowest line and remove the hashtags, then the other tags are available
# elif [$KB==$l3]
#      setxkbmap $l4

# elif [$KB==$l4]

#      setxkbmap $l1

fi
