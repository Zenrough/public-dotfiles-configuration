#!/usr/bin/env sh
# this script gives permission to kmonad to input 
# run this shell script as root/superuser

# adding default location of kmonad binary
echo "adding .local/bin/ to PATH"
PATH=/home/$1/:$PATH

# if you did not know $1 is 2.st argument
# so <echo nice> <echo> is $0 and nice is $1

# documentation for commands
# but understand yourself what they do, via
# https://github.com/kmonad/kmonad/blob/master/doc/faq.md#q-how-do-i-get-uinput-permissions
groupadd uinput

usermod -aG input $1
usermod -aG uinput $1


# add udev rule, look at the .rules file extenstion, it is required for the files to be read by udev
# !my understanding: this rule allows users in the uinput group to input userinput(uinput)
echo "adding kernel rule"
echo 'KERNEL=="uinput", MODE="0660", GROUP="uinput", OPTIONS+="static_node=uinput"' > /etc/udev/rules.d/80-kmonad.rules
echo "Script done"
